import * as React from "react";
import * as ReactDOM from "react-dom";

import Weather from "./weather";

ReactDOM.render(<Weather />, document.getElementById("Root"));
